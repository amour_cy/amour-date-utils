package com.amour.date.utils;

import cn.hutool.core.date.DateUtil;

public class DateUtils {

	public static String now() {
		return DateUtil.now();
	}
}